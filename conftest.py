from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import pytest
from pages.home_page import HomePage
from pages.search_page import SearchPage
import random
from allure_commons.types import AttachmentType
import allure


@pytest.fixture()
def driver():
    options = Options()
    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(options=options)
    yield driver
    # driver.save_screenshot(f'{random.randint(100, 50000)}.png')
    allure.attach(driver.get_screenshot_as_png(), name="Screenshot", attachment_type=AttachmentType.PNG)
    driver.quit()


@pytest.fixture()
def home_page(driver):
    return HomePage(driver)


@pytest.fixture()
def search_page(driver):
    return SearchPage(driver)
